#
# N(on)P(orn)D(atabase)
# Syr4c, 21.01.2016
# ct-design@gmx.de
#
# This class represent the database object, it contains the collection with the np-objects
# and the interfaces for the ui or other stuff
#
# Error Code: 1 - the object is already in the collection
# Error Code: 2 - cant find possible object to remove 
# Error Code: 3 - wrong parameter type only string or array
# Error Code: 4 - wrong parameter type only Set objects

require 'set'
require './has_np_object_error.rb'
require './has_not_np_object_error.rb'

class Database
	def initialize
		@dataset = Set.new
		@last_database_update
		set_last_database_update
	end

	# DB Operation Options
	def add_np_object(object)
		raise HasNpObjectError, "HasNpObjectError in #{p self} while add_np_object operation - Error Code: 1" if @dataset.include?(object)
		@dataset.add(object)
		set_last_database_update
		self
	end

	def remove_np_object(object)
		raise HasNotNpObjectError, "HasNotNpObjectError in #{p self} while remove_np_object operation - Error Code: 2" until @dataset.include?(object)
		@dataset.delete(object)
		set_last_database_update
		self
	end

	# Set and Get Methods
	def get_sorted_array(tags) # tags is an array with 'tags' to filter the objects could be also a string with only one tag
		temp_array = Array.new

		raise ArgumentError, "ArgumentError in #{p self} while get_sorted_array operation - Error Code: 3" until tags.is_a?(String) || tags.is_a?(Array)

		@dataset.each do |element|
			if tags.is_a?(String)
				element.get_tags.inlcude?(tags) ? temp_array.push(element) : nil
			else
				merg_tags = tags - element.get_tags
				merg_tags.length < tags.length ? temp_array.push(element) : nil
			end
		end
		temp_array.sort!
	end

	def get_database_last_update
		@last_database_update.dup
	end

	def get_database_set
		@dataset.dup
	end

	def set_database_set(database_set)
		raise ArgumentError, "ArgumentError in #{p self} while set_database_set operation - Error Code: 4" until dataset.is_a?(Set)
		@dataset = database_set
	end

	private
	def get_current_time_as_string
		Time.new.strftime("%d-%m-%Y %H:%M:%S")
	end

	def set_last_database_update
		@last_database_update = get_current_time_as_string
	end
end