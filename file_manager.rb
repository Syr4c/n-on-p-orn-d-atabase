#
# N(on)P(orn)D(atabase)
# Syr4c, 23.01.2016
# ct-design@gmx.de
#
# This class represent the file manager to load and reload database objects and
# create np_objects
#

require './np_object.rb'
require './database.rb'

class FileManager
	def initialize
		@all_files
		@database = Database.new
	end

	def check_if_dbdump_exist
		exist?("HEREDUMPFILEINPUT")
	end

	# procedure for find an save all vid_files recursively - just start if no binary file to load is given
	def check_for_files
		vid_files = File.join("**", "*.{flv,mp4,mpg,avi}")
		@all_files = Dir.glob(vid_files)
		np_object_creation
		#clean_up_names
	end

	# ToDO - Good parsing method to clean up the names
	def clean_up_names
	end

	def np_object_creation
		# For the first time i will use the file_path as file name, till i have a good parser which will refectore the name frome the file_paths
		@all_files.each do |file|
			@database.add_np_object(NonPornObject.new(file, file))
		end
	end

	# Dump Database to binary file
	def dump_database_file
		serialized_db = Marshal::dump(@database.get_database_set)
		File.open
	end
end