#
# N(on)P(orn)D(atabase)
# Syr4c, 21.01.2016
# ct-design@gmx.de
#
# This class represent an np-object, it contains various informations about
# filename, name, rating etc.
#
# .... and it is for sure not a porn object ;)
#
# @file_name == @file_path
#
# Error Code: 1 - given parameter is to long or had wrong arguments
# Error Code: 2 - given parameter is wrong object type
# Error Code: 3 - given parameter is wrong object type or just to long
#

class NonPornObject
	include Comparable

	def initialize(file_name, name)
		@file_name = file_name
		@name = name
		@rating = 0
		@tags = Array.new
		@added = get_current_time_as_string
		@last_viewed
	end

	# Collection finding methods
	def <=>(other)
		other.get_rating <=> @rating
	end

	def ==(other)
		[@file_name, @name] == [other.get_file_name, other.get_name]
	end

	def hash
		prime = 31
    	result = 1
    	result = prime*result + ((@file_name == nil) ? 0 : @file_name.hash)
    	result = prime*result + ((@name == nil) ? 0 : @name.hash)
	end 

	def eql?(other)
		return true if self.equal?(other)
		return true if other.is_a?(self) && [@file_name, @name] == [other.get_file_name, other.get_name]
		false
	end

	# Set and Get Methods
	def get_file_name
		@file_name.dup
	end

	def get_name
		@name.dup
	end

	def get_rating
		@rating
	end

	def get_tags
		@tags.dup
	end

	def get_added_date
		@added.dup
	end

	def get_last_viewed
		@last_viewed.dup
	end

	def set_name(name)
		raise ArgumentError, "ArgumentError in #{p self} while set_name operation - Error Code: 3" until name.is_a?(String) && name.length < 15
		@name = name
		self
	end

	def set_rating(rating)
		raise ArgumentError, "ArgumentError in #{p self} while set_rating operation - Error Code: 2" until rating.is_a?(Fixnum)
		raise ArgumentError, "ArgumentError in #{p self} while set_rating operation - Error Code: 1" if rating > 6 || rating <= -1
		@rating = rating
	end

	def set_last_viewed
		@last_viewed = get_current_time_as_string
	end

	# Tag operation methods, tag could be an array
	def add_tags(tag)
		if tag.is_a?(Array)
			@tags.push(tag).flatten!.uniq!
		else
			@tags.push(tag).uniq!
		end
		self
	end

	def remove_tags(tag)
		if tag.is_a?(Array)
			@tags = @tags - tag
		else
			@tags.delete(tag)
		end
		self
	end

	# overritten to_s method
	def to_s
		"Titel: #{@name}, Path: #{@filename}, last_viewed: #{@last_viewed}"
	end

	private
	def get_current_time_as_string
		Time.new.strftime("%d-%m-%Y %H:%M:%S")
	end
end
